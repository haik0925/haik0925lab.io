---
title: About me
comments: false
---

I am a junior at DigiPen Institute of Technology pursuing "BS in CS in RTIS", a specialized major that focuses on the computer science of Real-Time Interactive Simulation.

---
title: Graphics Notes
date: 2020-07-22
---

- In a non-reversed depth buffer, the bigger the depth value is, the farther the primitive is positioned.
- 
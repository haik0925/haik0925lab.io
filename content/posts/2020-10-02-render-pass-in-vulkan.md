---
title: Render Pass in Vulkan
date: 2020-10-02
---

# Render Pass
- Render targets (= Framebuffer attachments)
  - Size
  - Format
- Subpasses
  - How the RTs are used
  - What are performed using the RTs
  - Shares the same resolution and tile arrangement across other subpasses
